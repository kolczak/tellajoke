package com.kolczak.tellajoke.immersion;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.google.android.glass.widget.CardBuilder;
import com.kolczak.tellajoke.R;

/**
 * Created by Konrad Olczak on 15/11/14.
 */
public class AboutMeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(createLayout());
    }

    private View createLayout() {

        View view = new CardBuilder(this, CardBuilder.Layout.AUTHOR)
                .setText("Android and iOS software engineer. Enthusiastic of new devices like Google Glass and Android Wear.")
                .setIcon(R.drawable.me)
                .setHeading("Konrad Olczak")
                .setSubheading("Exact Business Software")
                .setFootnote("This is the footnote")
                .setTimestamp("just now")
                .getView();

        return view;
    }
}
