package com.kolczak.tellajoke.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class JokesManager implements HttpGetTask.HttpResponseHandler {

	private Context context;
	private ArrayList<Joke> jokes = new ArrayList<Joke>();
    private LoadJokesListener loadListener;

    public JokesManager(Context context) {
		this.context = context;
		loadJokes();
	}
	
	private void loadJokes() {		
		String jokesString = loadJokesContentFromAsset();
		try {
			parseJokes(jokesString);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void parseJokes(String jokesString) throws JSONException {
		JSONObject jokesJson;
		jokesJson = new JSONObject(jokesString);
		JSONArray results = jokesJson.getJSONArray("results");
		
		for (int i = 0; i < results.length(); i++) {
			JSONObject jokeJson = results.getJSONObject(i);
			
			Joke joke = new Joke();
			joke.category = jokeJson.getString("categories");
			joke.content = jokeJson.getString("content");
			
			jokes.add(joke);
		}
	}

	private String loadJokesContentFromAsset() {
        String json = null;
        try {
            InputStream is = this.context.getAssets().open("jokes.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
	
	public void loadRandomJoke(LoadJokesListener loadListener) {
        this.loadListener = loadListener;

        if (Utils.isConnected(this.context)) {
            new HttpGetTask(this).execute("http://api.icndb.com/jokes/random");
        } else {
            final int randomIndex = (int) Math.floor(Math.random()*jokes.size());
            this.loadListener.jokesLoaded(new ArrayList<Joke>() {{add(jokes.get(randomIndex));}});
        }
	}

    public void loadJokes(LoadJokesListener loadListener) {
        this.loadListener = loadListener;

        if (Utils.isConnected(this.context)) {
            new HttpGetTask(this).execute("http://api.icndb.com/jokes");
        } else {
            this.loadListener.jokesLoaded(this.jokes);
        }
    }

    @Override
    public void onHttpResponseError(String message) {
        this.loadListener.jokesLoadingError(message);
    }

    @Override
    public void onHttpResponseSuccess(JSONObject jsonObject) {
        List<Joke> jokes = parseJokes(jsonObject);
        if (jokes == null) {
            this.loadListener.jokesLoadingError("parsing error");
        } else {
            this.loadListener.jokesLoaded(jokes);
        }
    }

    private List<Joke> parseJokes(JSONObject jsonObject) {

        ArrayList<Joke> jokes = new ArrayList<Joke>();

        try {
            Object value = jsonObject.get("value");

            if (value instanceof JSONArray) {
                JSONArray jokesArray = (JSONArray)value;

                for (int i = 0; i < jokesArray.length(); i++) {
                    Joke joke = parseJoke(jokesArray.getJSONObject(i));
                    jokes.add(joke);
                }

            } else if (value instanceof JSONObject) {
                Joke joke = parseJoke((JSONObject) value);
                jokes.add(joke);
            } else {
                jokes = null;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jokes;
    }

    private Joke parseJoke(JSONObject value) throws JSONException {
        String jokeContent = value.getString("joke");

        Joke joke = new Joke();
        joke.category = "Chuck Norris";
        joke.content = Utils.addQuotes(jokeContent);
        return joke;
    }

    public interface LoadJokesListener {
        public void jokesLoaded(List<Joke> joke);
        public void jokesLoadingError(String message);
    }
}
