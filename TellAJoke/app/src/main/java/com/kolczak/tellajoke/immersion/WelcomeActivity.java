package com.kolczak.tellajoke.immersion;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.glass.view.WindowUtils;
import com.google.android.glass.widget.CardBuilder;
import com.kolczak.tellajoke.R;

/**
 * Created by Konrad Olczak on 15/11/14.
 */
public class WelcomeActivity extends GenericActivity {

    private final Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);

        View view = createLayout();

        setContentView(view);
    }

    private View createLayout() {

        View view = new CardBuilder(this, CardBuilder.Layout.TITLE)
                //.setText("Tell A Joke")
                //.setIcon(R.drawable.smile2)
                .addImage(R.drawable.chuck_norris_approved)
                .getView();

        return view;
    }

    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_menu_show_jokes:
                startActivityClass(JokesScrollActivity.class);
                break;
            case R.id.main_menu_about_me:
                startActivityClass(AboutMeActivity.class);
                break;
            case R.id.main_menu_send_feedback:
                startActivityClass(SendFeedbackActivity.class);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void startActivityClass(final Class<?> activityClass) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(WelcomeActivity.this, activityClass);
                startActivity(intent);
            }
        });
    }
}
