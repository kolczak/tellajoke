package com.kolczak.tellajoke.livecard;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.widget.RemoteViews;

import com.google.android.glass.timeline.LiveCard;
import com.google.android.glass.widget.CardBuilder;
import com.kolczak.tellajoke.common.Joke;
import com.kolczak.tellajoke.common.JokesManager;
import com.kolczak.tellajoke.R;

import java.util.List;

/**
 * @author Konrad Olczak
 * 
 */
public class JokeService extends Service implements JokesManager.LoadJokesListener {

	private static final String LIVE_CARD_ID = "Hello";
	private LiveCard mLiveCard;
	private TextToSpeech mSpeech;
	private final MainBinder mBinder = new MainBinder();
	private JokesManager mManager;
	private Joke mJoke;
	private RemoteViews mLiveCardView;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (mLiveCard == null) {
            mLiveCard = new LiveCard(this, LIVE_CARD_ID);
            setLoadingScreen(mLiveCard);
            mLiveCard.publish(LiveCard.PublishMode.REVEAL);
            loadJoke();
        } else if (!mLiveCard.isPublished()) {
            mLiveCard.publish(LiveCard.PublishMode.REVEAL);
        } else {
            mLiveCard.navigate();
        }
        return START_STICKY;
    }

    private void loadJoke() {
        if (mManager == null) {
            mManager = new JokesManager(getApplicationContext());
        }

        mManager.loadRandomJoke(this);
    }

    @Override
    public void jokesLoaded(List<Joke> jokes) {
        mJoke = jokes.get(0);

        mLiveCardView = getJokeLiveCardRemoteViews();

        Intent mIntent = new Intent(this, JokeActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mLiveCard.setAction(PendingIntent.getActivity(this, 0, mIntent, 0));

        mLiveCard.setViews(mLiveCardView);
    }

    private RemoteViews getJokeLiveCardRemoteViews() {
        return new CardBuilder(this, CardBuilder.Layout.TEXT)
                .addImage(R.drawable.chuck_norris5)
                .setText(mJoke.content)
                .getRemoteViews();
    }

    private RemoteViews getLoadingLiveCardRemoteViews() {
        return new CardBuilder(this, CardBuilder.Layout.TEXT)
                .setText(getString(R.string.loading))
                .getRemoteViews();
    }

    /**
	 * Binder is like a interface that allows others call its methods.
	 * @author konradolczak
	 *
	 */
	public class MainBinder extends Binder {
		public void tellAJoke() {
			mSpeech.speak(mJoke.content + ".\n\n" + getString(R.string.haha), TextToSpeech.QUEUE_FLUSH, null);
		}
		
		public void displayNextJoke() {
            setLoadingScreen(mLiveCard);
            loadJoke();
		}
    }

    private void setLoadingScreen(LiveCard mLiveCard) {
        mLiveCardView = getLoadingLiveCardRemoteViews();
        mLiveCard.setViews(mLiveCardView);
    }

    @Override
	public void onCreate() {
		super.onCreate();
		mSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
			@Override
			public void onInit(int status) {
				mSpeech.setSpeechRate(0.7f);
			}
		});
	}

    @Override
    public void jokesLoadingError(String message) {

    }

	@Override
	public void onDestroy() {
	    if (mLiveCard != null && mLiveCard.isPublished()) {
	        mLiveCard.unpublish();
	        mLiveCard = null;
	    }
	    mSpeech.shutdown();

        mSpeech = null;
        super.onDestroy();
	}

	/**
	 * Returns binder to activity, so activity can easily call it.
	 */
	@Override
	public IBinder onBind(Intent intent) {
		return mBinder ;
	}
}
