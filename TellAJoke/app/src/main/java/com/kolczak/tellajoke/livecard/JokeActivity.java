package com.kolczak.tellajoke.livecard;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;

import com.kolczak.tellajoke.R;

/**
 *
 * @author Konrad Olczak
 * 
 */
public class JokeActivity extends Activity {
	private boolean mAttachedToWindow;
	private boolean mResumed;
	private JokeService.MainBinder mService;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.live_card_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.read_aloud:
                mService.tellAJoke();
                return true;
            case R.id.next_joke:
                mService.displayNextJoke();
                return true;
            case R.id.stop:
                stopService(new Intent(this, JokeService.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName arg0, IBinder binderService) {
			if(binderService instanceof JokeService.MainBinder) {
				mService = (JokeService.MainBinder) binderService;
				openOptionsMenu();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			// TODO Auto-generated method stub
		}
	};
	
	@Override
    public void openOptionsMenu() {
        if (mResumed && mAttachedToWindow && mService != null) {
            super.openOptionsMenu();
        }
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);

        unbindService(mConnection);

        // We must call finish() from this method to ensure that the activity ends either when an
        // item is selected from the menu or when the menu is dismissed by swiping down.
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(mService == null) {
            bindService(new Intent(this, JokeService.class), mConnection, 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mResumed = true;

        if (mAttachedToWindow)
            openOptionsMenu();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mResumed = false;
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        mAttachedToWindow = true;
        openOptionsMenu();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mAttachedToWindow = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        finish();
    }

}
