package com.kolczak.tellajoke.immersion;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.WindowManager;

import com.google.android.glass.media.Sounds;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;

/**
 * Created by Konrad Olczak on 15/11/14.
 * Contains methods common for all activities.
 */
public abstract class GenericActivity extends Activity {

    protected GestureDetector mGestureDetector;
    protected AudioManager mAudioManager;
    protected boolean mKeepScreenOn = true;

    private final GestureDetector.BaseListener baseListener = new GestureDetector.BaseListener() {
        @Override
        public boolean onGesture(Gesture gesture) {
            if (gesture == Gesture.TAP) {
                mAudioManager.playSoundEffect(Sounds.TAP);
                onTap();
                return true;
            } else if (gesture == Gesture.LONG_PRESS) {
                mAudioManager.playSoundEffect(Sounds.TAP);
                onLongPress();
                return true;
            } else if(gesture == Gesture.SWIPE_LEFT){
                onSwipeLeft();
                return true;
            } else if(gesture == Gesture.SWIPE_RIGHT){
                onSwipeRight();
                return true;
            } else if(gesture == Gesture.SWIPE_DOWN){
                mAudioManager.playSoundEffect(Sounds.DISMISSED);
                return onSwipeDown();
            } else {
                return false;
            }
        }
    };

    protected void onTap() {
        //override if necessary
        openOptionsMenu();
    }

    protected boolean onSwipeDown() {
        return false;
    }

    protected void onSwipeLeft() {}

    protected void onSwipeRight() {}

    protected void onLongPress() {
        //override if necessary
        openOptionsMenu();
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {
        return mGestureDetector.onMotionEvent(event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mGestureDetector = new GestureDetector(this).setBaseListener(baseListener);

        super.onCreate(savedInstanceState);

        if (mKeepScreenOn) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }
}
