package com.kolczak.tellajoke.common;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Konrad Olczak on 12/11/14.
 */
public class HttpGetTask extends AsyncTask<String, Void, String> {

    private final HttpResponseHandler mResponseHandler;

    public HttpGetTask(HttpResponseHandler responseHandler) {
        mResponseHandler = responseHandler;
    }

    @Override
    protected String doInBackground(String... urls) {
        return GET(urls[0]);
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {

        JSONObject json = null;
        try {
            json = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (json != null) {
            mResponseHandler.onHttpResponseSuccess(json);
        } else {
            mResponseHandler.onHttpResponseError(result);
        }
    }

    public static String GET(String url){
        InputStream inputStream = null;
        String result = "";
        try {

            HttpClient httpclient = new DefaultHttpClient();

            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            inputStream = httpResponse.getEntity().getContent();

            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public interface HttpResponseHandler {
        public void onHttpResponseError(String message);
        public void onHttpResponseSuccess(JSONObject jsonObject);
    }
}
