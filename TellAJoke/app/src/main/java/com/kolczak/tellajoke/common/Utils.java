package com.kolczak.tellajoke.common;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Konrad Olczak on 12/11/14.
 */
public class Utils {

    public static boolean isConnected(Context context){
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static String addQuotes(String stringQuoteless) {
        return stringQuoteless.replaceAll("&quot;", "\"");
    }
}
