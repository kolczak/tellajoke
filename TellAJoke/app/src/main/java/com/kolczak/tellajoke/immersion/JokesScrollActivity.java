package com.kolczak.tellajoke.immersion;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.glass.view.WindowUtils;
import com.google.android.glass.widget.CardBuilder;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;
import com.google.android.glass.widget.Slider;
import com.kolczak.tellajoke.common.Joke;
import com.kolczak.tellajoke.common.JokesManager;
import com.kolczak.tellajoke.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Konrad Olczak on 15/11/14.
 */
public class JokesScrollActivity extends GenericActivity implements JokesManager.LoadJokesListener {

    private List<CardBuilder> mCards;
    private CardScrollView mCardScrollView;
    private JokeCardScrollAdapter mAdapter;
    private List<Joke> mJokes;
    private Slider.Indeterminate mIndeterminate;
    private TextToSpeech mTextToSpeech;
    private boolean mShowVoiceCommands = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);

        mTextToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                mTextToSpeech.setSpeechRate(0.7f);
            }
        });

        mCardScrollView = new CardScrollView(this);

        Slider slider = Slider.from(mCardScrollView);
        mIndeterminate = slider.startIndeterminate();

        setContentView(mCardScrollView);

        JokesManager jokesManager = new JokesManager(this);
        jokesManager.loadJokes(this);
    }

    @Override
    public void jokesLoaded(List<Joke> jokes) {
        mJokes = jokes;
        createCards();

        mIndeterminate.hide();

        mAdapter = new JokeCardScrollAdapter();
        mCardScrollView.setAdapter(mAdapter);
        mCardScrollView.activate();

        mShowVoiceCommands = true;
        getWindow().invalidatePanelMenu(WindowUtils.FEATURE_VOICE_COMMANDS);
    }

    private void createCards() {

        mCards = new ArrayList<CardBuilder>();

        Joke joke;

        for (int i = 0; i < mJokes.size(); i++) {

            joke = mJokes.get(i);

            //----------- TEXT
            if (i%3 == 0) {
                mCards.add(
                        new CardBuilder(this, CardBuilder.Layout.TEXT)
                                .setText(joke.content)
                                .setFootnote(joke.category)
                                .addImage(R.drawable.chuck_norris1)
                                .setAttributionIcon(R.drawable.smile2)
                                .setTimestamp("just now")
                        //.showStackIndicator(true)

                );
            }

            //------------ CAPTION
            if (i%3 == 1) {
                mCards.add(
                        new CardBuilder(this, CardBuilder.Layout.CAPTION)
                                .setText(joke.content)
                                .setFootnote(joke.category)
                                .addImage(R.drawable.chuck_norris2)
                                .setIcon(R.drawable.chuck_norris3)
                                .setAttributionIcon(R.drawable.smile2)
                                .setTimestamp("just now")
                );
            }

            //------------ COLUMNS
            if (i%3 == 2) {
                mCards.add(
                        new CardBuilder(this, CardBuilder.Layout.COLUMNS)
                                .setText(joke.content)
                                .setFootnote(joke.category)
                                .addImage(R.drawable.chuck_norris1)
                                .addImage(R.drawable.chuck_norris2)
                                .addImage(R.drawable.chuck_norris3)
                                .addImage(R.drawable.chuck_norris4)
                                .addImage(R.drawable.chuck_norris5)
                                .setTimestamp("just now")
                                .setAttributionIcon(R.drawable.smile2)
                );
            }
        }

    }

    @Override
    public void jokesLoadingError(String message) {

    }

    @Override
    public boolean onPreparePanel(int featureId, View view, Menu menu) {
        if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS) {
            // Dynamically decides between enabling/disabling voice menu.
            return mShowVoiceCommands;
        }
        // Good practice to pass through, for options menu.
        return super.onPreparePanel(featureId, view, menu);
    }

    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        getMenuInflater().inflate(R.menu.card_scroll_menu, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.read_aloud:
                tellAJoke();
                return true;
            case R.id.next_joke:
                showNextJoke();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showNextJoke() {
        int position = mCardScrollView.getSelectedItemPosition();
        mCardScrollView.animate(position + 1, CardScrollView.Animation.NAVIGATION);
    }

    public void tellAJoke() {
        int currentJokePosition = mCardScrollView.getSelectedItemPosition();

        mTextToSpeech.speak(mJokes.get(currentJokePosition).content + ".\n\n" + getString(R.string.haha), TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onDestroy() {
        mTextToSpeech.shutdown();
        mTextToSpeech = null;
        super.onDestroy();
    }

    private class JokeCardScrollAdapter extends CardScrollAdapter {

        @Override
        public int getPosition(Object item) {
            return mCards.indexOf(item);
        }

        @Override
        public int getCount() {
            return mCards.size();
        }

        @Override
        public Object getItem(int position) {
            return mCards.get(position);
        }

        @Override
        public int getViewTypeCount() {
            return 3;
        }

        @Override
        public int getItemViewType(int position){
            return mCards.get(position).getItemViewType();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = mCards.get(position).getView(convertView, parent);

            return view;
        }
    }
}
