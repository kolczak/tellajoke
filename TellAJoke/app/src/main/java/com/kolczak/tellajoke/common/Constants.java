package com.kolczak.tellajoke.common;

/**
 * Created by Konrad Olczak on 11/11/14.
 */
public class Constants {
    public static final boolean DEBUG = false;
    public static final String LOG_TAG = "Tell a Joke";
}
