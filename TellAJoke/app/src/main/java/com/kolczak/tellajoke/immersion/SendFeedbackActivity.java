package com.kolczak.tellajoke.immersion;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;

import com.google.android.glass.widget.CardBuilder;
import com.google.android.glass.widget.Slider;
import com.kolczak.tellajoke.common.Constants;

import java.util.ArrayList;

/**
 * Created by Konrad Olczak on 15/11/14.
 */
public class SendFeedbackActivity extends Activity {

    private int VOICE_RECOGNITION_REQUEST_CODE = 100;

    private Handler mHandler = new Handler();
    private Slider mSlider;
    private Slider.Indeterminate mIndeterminate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(createLayout());

        startVoiceRecognitionActivity();
    }

    private View createLayout() {

        View view = new CardBuilder(this, CardBuilder.Layout.MENU)
                .setText("Saving feedback")
//                .setFootnote("Optional menu description")
                .getView();

        mSlider = Slider.from(view);
        

        return view;
    }

    private void startVoiceRecognitionActivity() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
//	    intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, 5000);
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS, 6000);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Tell me what you think about the app");
        startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK) {
            // Fill the list view with the strings the recognizer thought it could have heard
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

            if (matches.size() == 1) {
                saveNote(matches.toString());
            }
            else {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startVoiceRecognitionActivity();
                    }
                }, 1000);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }



    private void saveNote(String noteContent) {

        try {
            mIndeterminate = mSlider.startIndeterminate();
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    onFeedbackSaved();
                }
            }, 3000);

        } catch (Exception e) {
            Log.w(Constants.LOG_TAG, "create note exception");
            e.printStackTrace();
        }
    }

    private void onFeedbackSaved() {
        mIndeterminate.hide();
        finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
