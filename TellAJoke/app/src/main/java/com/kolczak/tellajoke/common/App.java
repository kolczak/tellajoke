package com.kolczak.tellajoke.common;

import android.app.Application;
import android.util.Log;

/**
 * Created by Konrad Olczak on 11/11/14.
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        if (Constants.DEBUG) {
            android.os.Debug.waitForDebugger();
            Log.d(Constants.LOG_TAG, "start debugging");
        }
    }
}
